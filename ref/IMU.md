﻿# IMU

## PJRC (Teensy) prop shield

[website](https://www.pjrc.com/store/prop_shield.html)

FXOS8700CQ - 6-Axis Sensor, Linear Accelerometer & Magnetometer
FXAS21002C - 3-Axis Digital Angular Rate Gyroscope
MPL3115A2 - Precision Pressure/Altitude & Temperature

## Pololu imu01b

MinIMU-9 v2 Gyro, Accelerometer, and Compass (L3GD20 and LSM303DLHC Carrier)

(discontinued)

[website](https://www.pololu.com/product/1268)

currently working in minion.

## GY-85 9DOF-BB

9DOF ITG3205 ADXL345 HMC5883L

[website](http://www.hobbytronics.co.uk/sensors/imu/9dof-breakout)

- ITG3205 3-axis Gyro
- ADXL345 3-axis Accelerometer
- HMC5883L 3-axis Magnetometer

[Arduino example](http://playground.arduino.cc/Main/SEN-10724)

currently in wemo test board + 1 spare

## GY-91

10DOF MPU-9250 and BMP280 Multi-Sensor Module

[website](https://artofcircuits.com/product/10dof-gy-91-4-in-1-mpu-9250-and-bmp280-multi-sensor-module)

- MPU-9250 inc MPU-6500 (gyro/accel) AK8963 (mag)
- BMP280 temp/baro

## 10 DOF ebay - STK0151004902

- LSM303DLHC accel/mag
- L3GD20 gyro
- BMP180 temp/barometer
