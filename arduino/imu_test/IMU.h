#pragma once

class IMU
{
public:
	typedef struct vector
	{
		float x, y, z;
	} vector;

	// vector functions
	static void vector_cross(const vector *a, const vector *b, vector *out);
	static float vector_dot(const vector *a, const vector *b);
	static void vector_normalize(vector *a);

private:
};
