// ADXL345 3-axis Accelerometer
#pragma once

#include <Wire.h>
#include "vector.h"

#define ADXL345_ADDR 0x53

class ADXL345
{
public:
	ADXL345()
	{
	}

	union XYZBuffer a;

	void setup()
	{
		// Check ID to see if we are communicating
		Wire.beginTransmission(ADXL345_ADDR);
		Wire.write(0x00); // One Reading
		Wire.endTransmission();
		Wire.requestFrom(ADXL345_ADDR, 1);
		while (!Wire.available());
		byte ch = Wire.read();
		Serial.print("Accel id is 0x");
		Serial.println(ch, HEX);
		// Should output E5

		// https://www.sparkfun.com/datasheets/Sensors/Accelerometer/ADXL345.pdf
		// Page 16
		Wire.beginTransmission(ADXL345_ADDR);
		Wire.write(0x2d);
		Wire.write(0x08);
		Wire.endTransmission();
		Wire.beginTransmission(ADXL345_ADDR);
		Wire.write(0x38);
		Wire.write(0x84);
		Wire.endTransmission();
	}

	void read()
	{
		Wire.beginTransmission(ADXL345_ADDR);
		Wire.write(0x32); // One Reading
		Wire.endTransmission();
		readXYZ(ADXL345_ADDR, &a);
	}

	float pitch()
	{
		// scale by 1/000 as reading is 1mg per LSB

		/*
		float ax = (a.value.x >> 4);
		float ay = (a.value.y >> 4);
		float az = (a.value.z >> 4);
		*/
		float ax = (a.value.x);
		float ay = (a.value.y);
		float az = (a.value.z);

		float p = (atan2(ax, sqrt(ay*ay + az*az)) * 180.0) / M_PI;

		if (az > 0) p = -(180 + p);

		return p;
	}

	float roll()
	{
		// scale by 1/000 as reading is 1mg per LSB
		/*
		float ay = (a.value.y >> 4);
		float az = (a.value.z >> 4);
		*/
		float ay = (a.value.y);
		float az = (a.value.z);

		return (atan2(ay, -az) * 180.0) / M_PI;
	}


private:

	// Generically useful reading into a union type
	void readXYZ(int device, union XYZBuffer *xyz) {
		Wire.requestFrom(device, 6);
		long start = millis();
		while (!Wire.available() && (millis() - start)<100);
		if (millis() - start<100) {
			for (int i = 0; i<6; i++)
				xyz->buff[i] = Wire.read();
		}
	}



};