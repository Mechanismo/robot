/*
    Name:       imu_test.ino
    Created:	30/12/2018 13:52:01
    Author:     10bulls
*/

#include <Wire.h>

#include "ITG3205.h"
#include "ADXL345.h"
#include "HMC5883L.h"

ITG3205 gyro;
ADXL345 accel;
HMC5883L compass;

unsigned long time_us;
float yaw=0, pitch=0, roll=0;

// #define BUTTON1_PIN 12
// #define BUTTON2_PIN 13
#define BUTTON1_PIN D3
#define BUTTON2_PIN D4

//
void setup()
{
	Serial.begin(115200);
	delay(100);
	Wire.begin();

	Serial.println("gyro init...");
	gyro.setup();
	Serial.println("accel init...");
	accel.setup();
	Serial.println("compass init...");
	compass.setup();
	Serial.println("IMU OK");

	accel.read();
	yaw = 0;
	pitch = accel.pitch();
	roll = accel.roll();
	time_us = micros();

	// not sure if needed
	// gyro.read();
	pinMode(BUTTON1_PIN, INPUT_PULLUP);
	pinMode(BUTTON2_PIN, INPUT_PULLUP);
}

//
void loop()
{
	CheckButtons();

	gyro.read();

	/*
	Serial.print("g.X=");
	Serial.print(gyro.hx);
	Serial.print(" g.Y=");
	Serial.print(gyro.hy);
	Serial.print(" g.Z=");
	Serial.print(gyro.hz);
	*/
	/*
	Serial.print("g.X=");
	Serial.print(gyro.rawx);
	Serial.print(" g.Y=");
	Serial.print(gyro.rawy);
	Serial.print(" g.Z=");
	Serial.print(gyro.rawz);
	*/
	//Serial.print(" g.F=");
	//Serial.print(gyro.turetemp);
	//Serial.print((char)223);
	//Serial.print("C");

	
	/*
	Serial.print(" g.X=");
	Serial.print(gyro.hx);
	Serial.print(" g.Y=");
	Serial.print(gyro.hy);
	Serial.print(" g.Z=");
	Serial.print(gyro.hz);
	Serial.print(" g.F=");
	Serial.print(gyro.turetemp);
	Serial.print((char)223);
	Serial.print("C");
	*/

	// compass.read();
	/*
	Serial.print(" c.X=");
	Serial.print(compass.c.value.x);
	Serial.print(" c.Y=");
	Serial.print(compass.c.value.y);
	Serial.print(" c.Z=");
	Serial.print(compass.c.value.z);
	*/

	accel.read();

	unsigned long us = micros();
	float dt = us - time_us;
	time_us = us;

	// degree change from gyros since last read
	float dpitch = (float)(gyro.hx) * (float)dt / 1.0e6;
	float droll = (float)(gyro.hy) * (float)dt / 1.0e6;
	float dyaw = (float)(gyro.hz) * (float)dt / 1.0e6;
	
	// complementary filter... (combine gyro change with accelerometer data)
	pitch = 0.95 * (pitch + dpitch) + 0.05 * accel.pitch();
	roll = 0.95 * (roll + droll) + 0.05 * accel.roll();

	// TODO : get heading from compass
	yaw += dyaw;

	print_HeadPitchRoll();

	Serial.println("");

	// delay(50);
}

bool CheckButtons()
{
	static int button1_pin_state = 1;
	static int button2_pin_state = 1;

	int state = digitalRead(BUTTON1_PIN);
	if (state != button1_pin_state)
	{
		delay(50);
		button1_pin_state = state;
		if (button1_pin_state == 0)
		{
			// button 1 action
			yaw = 0;
			return true;
		}
	}

	state = digitalRead(BUTTON2_PIN);
	if (state != button2_pin_state)
	{
		delay(50);
		button2_pin_state = state;
		if (button2_pin_state == 0)
		{
			// button 1 action
			return true;
		}
	}
	return false;
}


void print_HeadPitchRoll()
{
	/*
	Serial.print(" a.X=");
	Serial.print(accel.a.value.x);
	Serial.print(" a.Y=");
	Serial.print(accel.a.value.y);
	Serial.print(" a.Z=");
	Serial.print(accel.a.value.z);
	*/
	// accelerometer readings only...
	/*
	Serial.print(yaw);
	Serial.print(" ");
	Serial.print(accel.pitch());
	Serial.print(" ");
	Serial.print(accel.roll());
	*/
	Serial.print(yaw);
	Serial.print(" ");
	Serial.print(pitch);
	Serial.print(" ");
	Serial.print(roll);
	//Serial.println("");

}
