// HMC5883L 3-axis Magnetometer
#pragma once

#include <Wire.h>
#include "vector.h"

#define HMC5883L_ADDR 0x1e

class HMC5883L
{
public:
	HMC5883L()
	{
	}

	union XYZBuffer c;

	void setup()
	{
		// Check ID to see if we are communicating
		Wire.beginTransmission(HMC5883L_ADDR);
		Wire.write(10); // One Reading
		Wire.endTransmission();
		Wire.requestFrom(HMC5883L_ADDR, 2);
		while (!Wire.available());
		char ch = Wire.read();
		Serial.print(ch);
		ch = Wire.read();
		Serial.println(ch);
		// Should output H4  

		// Page 18
		// at http://dlnmh9ip6v2uc.cloudfront.net/datasheets/Sensors/Magneto/HMC5883L-FDS.pdf
		Wire.beginTransmission(HMC5883L_ADDR);
		Wire.write(0x00); Wire.write(0x70);
		Wire.endTransmission();
		Wire.beginTransmission(HMC5883L_ADDR);
		Wire.write(0x01); Wire.write(0xA0);
		Wire.endTransmission();
		Wire.beginTransmission(HMC5883L_ADDR);
		Wire.write(0x02); Wire.write(0x00); //  Reading
		Wire.endTransmission();
		delay(6);
	}
	
	void read() 
	{
		readXYZ(HMC5883L_ADDR, &c);
		changeEndian(&c);
		Wire.beginTransmission(HMC5883L_ADDR);
		Wire.write(0x03);
		Wire.endTransmission();
	}

private:
	// Generically useful reading into a union type
	void readXYZ(int device, union XYZBuffer *xyz) {
		Wire.requestFrom(device, 6);
		long start = millis();
		while (!Wire.available() && (millis() - start)<100);
		if (millis() - start<100) {
			for (int i = 0; i<6; i++)
				xyz->buff[i] = Wire.read();
		}
	}

	void changeEndian(union XYZBuffer *xyz) {
		for (int i = 0; i<6; i += 2) {
			byte t = xyz->buff[i];
			xyz->buff[i] = xyz->buff[i + 1];
			xyz->buff[i + 1] = t;
		}
	}

};
